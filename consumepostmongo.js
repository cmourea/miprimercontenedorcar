const URL = "https://api.mlab.com/api/1/databases/carlosbootcamp/collections/posts?apiKey=nnsV0D6jJTUZY-cBYomezF6JwgETCUjl"
var response;

function obtenerPosts(){
  var peticion = new XMLHttpRequest();
  peticion.open("GET", URL, false);
  peticion.setRequestHeader("Content-Type","application/json");
  peticion.send();
  response = JSON.parse(peticion.responseText);
  sessionStorage["posts"] = peticion.responseText;
  console.log(response);

  // mostrarPost();

};

function mostrarPost() {

  var tabla = document.getElementById("tablaPosts");

  for (var i = 0; i < response.length; i++) {

    //alert(response[i].Titulo);

    var fila = tabla.insertRow(i+1);
    var celdaId = fila.insertCell(0);
    var celdaTitulo = fila.insertCell(1);
    var celdaTexto = fila.insertCell(2);
    var celdaAutor = fila.insertCell(3);
    var celdaOperaciones = fila.insertCell(4);
    var celdaBorrar = fila.insertCell(5);

    celdaId.innerHTML = response[i]._id.$oid;
    celdaTitulo.innerHTML = response[i].Titulo;
    celdaTexto.innerHTML = response[i].Texto;
    if(response[i].Autor != undefined)
    {
      celdaAutor.innerHTML = response[i].Autor.Nombre + " " + response[i].Autor.Apellido;
    }
    else
    {
      celdaAutor.innerHTML = "Anónimo";
    };
    celdaOperaciones.innerHTML = '<button onclick=\'actualizarPost("' + celdaId.innerHTML + '")\';>Actualizar</button>';
    celdaBorrar.innerHTML = '<button onclick=\'borrarPost("' + celdaId.innerHTML + '")\';>Borrar</button>';
  };

};


function anadirPost() {
  var peticion = new XMLHttpRequest();
  peticion.open("POST", URL, false);
  peticion.setRequestHeader("Content-Type","application/json");
  peticion.send('{"Titulo":"Nuevo POST desde ATOM", "Texto": "Nuevo texto desde ATOM","Autor":{"Nombre":"Car","Apellido":"Mou"}}');
}

function actualizarPost(id) {
  var peticion = new XMLHttpRequest();
  var URLItem = "https://api.mlab.com/api/1/databases/carlosbootcamp/collections/posts/";
  URLItem += id;
  URLItem += "?apiKey=nnsV0D6jJTUZY-cBYomezF6JwgETCUjl";
  peticion.open("PUT", URLItem, false);
  peticion.setRequestHeader("Content-Type","application/json");
  peticion.send('{"Titulo":"Título cambiado"}');
}

function borrarPost(id) {
  var peticion = new XMLHttpRequest();
  var URLItem = "https://api.mlab.com/api/1/databases/carlosbootcamp/collections/posts/";
  URLItem += id;
  URLItem += "?apiKey=nnsV0D6jJTUZY-cBYomezF6JwgETCUjl";
  peticion.open("DELETE", URLItem, false);
  peticion.setRequestHeader("Content-Type","application/json");
  peticion.send({});

}

function seleccionarPost(numero) {
  sessionStorage["seleccionado"] = numero;
}

function buscarDetallesPost(numero){
  var posts = JSON.parse(sessionStorage["posts"]);
  for (var i = 0; i < posts.length; i++) {
    if (posts[i]._id.$oid == numero)
    {
      //Mostrar detalles
      document.getElementById("h1").innerHTML = numero;
      document.getElementById("h2").innerHTML = posts[i].Titulo;
      document.getElementById("h3").innerHTML = posts[i].Texto;
      break;
    }
  }
}
