var expect = require('chai').expect;
var $ = require('chai-jquery');
var request = require('request');

describe("Pruebas sencillas", function() {

  it('Test suma', function() {
    expect(9+4).to.equal(13);
  })
});

/*it('Test Internet', function(done) {
  request.get("http://www.forocoches.com",
              function(error, response, body) {
                expect(response.statusCode).to.equal(200);
                done();
  });
});
*/

describe("Pruebas complicadas", function() {
  it('Test Conectividad Blog', function(done) {
    request.get("http://localhost:8082",
                function(error, response, body) {
                  expect(response.statusCode).to.equal(200);
                  done();
                });
  });

  it('Test Chequeo Título', function(done) {
    request.get("http://localhost:8082",
                function(error, response, body) {
                  expect(body).to.contain("<h1>Bienvenido a mi blog</h1>");
                  done();
                });
  });
});

/* describe("Test contenido HTML", function(done) {
  it('Test H1', function(done){
    request.get("http://localhost:8082",
                function(error, response, body) {
                  expect($('body h1')).to.have.text("Bienvenido a mi blog");
    //              done();
                });
  });
});
*/

describe("Test contenido html", function (done) {

  it('Test H1', function () {

    request.get("http://localhost:8081", function (error, res, body) {

      // console.log(body);

      expect($('body h1')).to.have.text("Bienvenido a mi blog");
      done();

    });

  });
});
